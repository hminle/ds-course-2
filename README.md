# Data Science Course 2
## Link Colab

1. Colab HW 1 link: https://colab.research.google.com/drive/1jb8ETVtvcbU7MVkEYH1J_rW8f12ddWGt
2. Colab HW 2 link: https://colab.research.google.com/drive/1aApZVWjESqyXOI3p3koTRvx-F2bVxLUL
Currently, I cannot run my code on colab because of conflicted version of sklearn. You should check my jupyter notebook instead.
